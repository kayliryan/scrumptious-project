from django.urls import path

from tags.views import TagListView, TagDetailView
# from tags.views import show_tags


urlpatterns = [
    # path("", show_tags, name="tags_list"),
    path("", TagListView.as_view(), name="tags_list"),
    path("<int:pk>/", TagDetailView.as_view(), name="tag_detail"),
]
