from django.urls import path

from recipes.views import (
    # create_recipe,
    # change_recipe,
    log_rating,
    # show_recipe,
    # show_recipes,
    RecipeListView,
    RecipeDetailView,
    RecipeCreateView,
    RecipeUpdateView,
    RecipeDeleteView,
)

urlpatterns = [
    path("", RecipeListView.as_view(), name="recipes_list"),
    # DELETED path("", show_recipes, name="recipes_list"),
    path("<int:pk>/", RecipeDetailView.as_view(), name="recipe_detail"),
    #this first argument is saying that it should be an int, and it will be a
    #primary key that it's looking up to find that data 
    # DELETED path("<int:pk>/", show_recipe, name="recipe_detail"),
    path("new/", RecipeCreateView.as_view(), name = "recipe_new"),
    #path("new/", create_recipe, name="recipe_new"),
    path("<int:pk>/update", RecipeUpdateView.as_view(), name="recipe_edit"),
    # path("edit/", change_recipe, name="recipe_edit"),
    path("<int:pk>/delete", RecipeDeleteView.as_view(), name = "recipe_delete"),
    path("<int:recipe_id>/ratings/", log_rating, name="recipe_rating"),
]

#the first parameter is what actually shows up in the url from that main page.
#recipes is already there in the main page hence the "" in our RecipeListView path