from django.shortcuts import redirect
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from recipes.forms import RatingForm

try:
    from recipes.forms import RecipeForm
    from recipes.models import Recipe
except Exception:
    RecipeForm = None
    Recipe = None

class RecipeCreateView(CreateView):
    model = Recipe
    fields = ["name", "author", "description", "image"]
    #these field names have to be the same as the variable names in your model
    template_name = "recipes/new.html"
    success_url = "/"
    #where it will redirect the user after it's created
    #in this case they'll go back to the index page and be back on the list (main) view page
    # / always goes back to the index page

#REPLACED WITH RECIPECREATEVIEW
# def create_recipe(request):
#     if request.method == "POST" and RecipeForm:
#         form = RecipeForm(request.POST)
#         if form.is_valid():
#             recipe = form.save()
#             return redirect("recipe_detail", pk=recipe.pk)
#     elif RecipeForm:
#         form = RecipeForm()
#     else:
#         form = None
#     context = {
#         "form": form,
#     }
#     return render(request, "recipes/new.html", context)

class RecipeUpdateView(UpdateView):
    model = Recipe
    fields = ["name", "author", "description", "image"]
    template_name = "recipes/edit.html"
    success_url = "/"


# #  REPLACED WITH RECIPEUPDATEVIEW
# def change_recipe(request, pk):
#     if Recipe and RecipeForm:
#         instance = Recipe.objects.get(pk=pk)
#         if request.method == "POST":
#             form = RecipeForm(request.POST, instance=instance)
#             if form.is_valid():
#                 form.save()
#                 return redirect("recipe_detail", pk=pk)
#         else:
#             form = RecipeForm(instance=instance)
#     else:
#         form = None
#     context = {
#         "form": form,
#     }
#     return render(request, "recipes/edit.html", context)


class RecipeListView(ListView):
    model = Recipe
    context_object_name = "recipes"
    template_name = "recipes/list.html"
    paginate_by = 4

    def get_queryset(self):
        query_string = self.request.GET.get("q")
        if not query_string:
            query_string = ""
        return Recipe.objects.filter(description__icontains=query_string)

#  REPLACED WITH RECIPELISTVIEW
# def show_recipes(request):
#     context = {
#         "recipes": Recipe.objects.all() if Recipe else [],
#     }
#     return render(request, "recipes/list.html", context)



class RecipeDetailView(DetailView):
    model = Recipe #where it's getting the data from
    context_object_name = "recipe" #can find this in your detail html file
    template_name = "recipes/detail.html" #The template we would like to be rendered when we go to a detail view

#  REPLACED WITH RECIPEDETAILVIEW
# def show_recipe(request, pk):
#     context = {
#         "recipe": Recipe.objects.get(pk=pk) if Recipe else None,
#         "rating_form": RatingForm(),
#     }
#     return render(request, "recipes/detail.html", context)



class RecipeDeleteView(DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = "/"


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            rating.recipe = Recipe.objects.get(pk=recipe_id)
            rating.save()
    return redirect("recipe_detail", pk=recipe_id)
