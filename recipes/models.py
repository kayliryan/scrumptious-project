from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator


# Create your models here.
class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField(
        null=True, blank=True
    )  # In our database this is going to say it's okay if this field is empty
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Step(models.Model):
    recipe = models.ForeignKey(
        "Recipe", related_name="steps", on_delete=models.CASCADE
    )
    order = models.PositiveSmallIntegerField()
    directions = models.CharField(max_length=300)
    food_items = models.ManyToManyField("FoodItem", blank=True)
    # this Foreign Key makes the relation between Steps and Recipe

    def __str__(self):
        return str(self.order)


class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)
    # unique means that it can't have two of the same value so if 1 name is cup another cant be

    def __str__(self):
        return self.name


class FoodItem(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    amount = models.FloatField(
        default=1, validators=[MaxValueValidator(20), MinValueValidator(1)]
    )
    recipe = models.ForeignKey(
        "Recipe", related_name="ingredients", on_delete=models.CASCADE
    )
    # What would that do?
    measure = models.ForeignKey("Measure", on_delete=models.PROTECT)
    # why doesn't it need a related name?
    food = models.ForeignKey("FoodItem", on_delete=models.PROTECT)

    def __str__(self):
        return f"{str(self.amount)} {self.measure.name} {self.food.name}"


class Rating(models.Model):
    value = models.PositiveSmallIntegerField(
        validators=[MaxValueValidator(5), MinValueValidator(1)]
    )
    recipe = models.ForeignKey("Recipe", related_name="ratings", on_delete=models.CASCADE,)
    def __str__(self):
        return f"Rating: {self.value}"
