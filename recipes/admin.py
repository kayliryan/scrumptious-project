from django.contrib import admin
from recipes.models import Recipe, Step, Measure, FoodItem, Ingredient, Rating
from tags.models import Tag


# Register your models here.
class RecipeAdmin(admin.ModelAdmin):
    pass


admin.site.register(Recipe, RecipeAdmin)


class StepAdmin(admin.ModelAdmin):
    pass


admin.site.register(Step, StepAdmin)


class MeasureAdmin(admin.ModelAdmin):
    pass


admin.site.register(Measure, MeasureAdmin)


class FoodItemAdmin(admin.ModelAdmin):
    pass


admin.site.register(FoodItem, FoodItemAdmin)


class IngredientAdmin(admin.ModelAdmin):
    pass


admin.site.register(Ingredient, IngredientAdmin)


class TagAdmin(admin.ModelAdmin):
    pass


admin.site.register(Tag, TagAdmin)


class RatingAdmin(admin.ModelAdmin):
    pass


admin.site.register(Rating, RatingAdmin)
